import openpyxl 

YEAR_RANGE = range(2017, 2024)

class BusinnesYear(object):
    def __init__(self, ws=None, start_cell=None, year=None, init_from_ws=True):
        self.year = year
        self._start_cell = start_cell

        self.q1 = 0
        self.q2 = 0
        self.q3 = 0
        self.q4 = 0
        self.total = None 

        if init_from_ws:
            self.q1 = ws.cell(row=start_cell.row, column=start_cell.col_idx + 1).value or 0 
            self.q2 = ws.cell(row=start_cell.row, column=start_cell.col_idx + 2).value or 0 
            self.q3 = ws.cell(row=start_cell.row, column=start_cell.col_idx + 3).value or 0 
            self.q4 = ws.cell(row=start_cell.row, column=start_cell.col_idx + 4).value or 0 

            self.calc_total()
        
    def calc_total(self):
        if isinstance(self._start_cell, openpyxl.cell.cell.Cell) and isinstance(self._start_cell.value, (int, long, float)):
            self.total = self._start_cell.value
        else:
            self.total = sum([self.q1, self.q2, self.q3, self.q4])
        
    def __repr__(self):
        if sum([self.q1, self.q2, self.q3, self.q4]) != 0:
            qs = ' qs: ' + ';'.join([str(self.q1), str(self.q2), str(self.q3), str(self.q4)])
        else:
            qs = ''
        return 'BusinnesYear: ' + str(self.year) + ' total: ' + str(self.total) + qs
        
class YearsRow(object):
    def __init__(self, ws=None, start_cell=None, init_from_ws=True):
        self.name = None
        self._start_cell = start_cell
        self.years = []
        
        if init_from_ws:
            self.name = ws.cell(row=start_cell.row, column=start_cell.col_idx).value
    
            i = 0
            for year in YEAR_RANGE:
                self.years.append(BusinnesYear(ws, ws.cell(row=start_cell.row, column = start_cell.col_idx + 4 + (5 * i)), year))
                i += 1
                
    def __repr__(self):
        return "\r\n" + str(self.__class__.__name__) + ": " + self.name + "\r\n" + "\r\n".join([str(y) for y in self.years])

class ProductRow(YearsRow):
    def __init__(self, ws=None, start_cell=None, init_from_ws=True):
        super(ProductRow, self).__init__(ws, start_cell, init_from_ws=init_from_ws)
        self.KW = None
        self.price_per_wat = None 
        self.price = None 
        self.years = []
        
        if init_from_ws:
            self.KW = ws.cell(row=self._start_cell.row, column=self._start_cell.col_idx + 1).value
            self.price_per_wat = ws.cell(row=self._start_cell.row, column=self._start_cell.col_idx + 2).value
            self.price = self.KW * self.price_per_wat 
            
            i = 0
            for year in YEAR_RANGE:
                self.years.append(BusinnesYear(ws, ws.cell(row=start_cell.row, column = start_cell.col_idx + 4 + (5 * i)), year))
                i += 1

class InputsSheet(object):
    def __init__(self, ws):
        self._ws = ws
        self.general_dict = self.parse_general_table()
        self.products = self.parse_products()
        
        self.revenue_from_custome_lasers = YearsRow(ws, ws['c20'])
        self.sales_margin = YearsRow(ws, ws['c22'])
        self.projects_margin = YearsRow(ws, ws['c23'])

        self.rnd = YearsRow(ws, ws['c27'])
        self.fixed_manufactoring = YearsRow(ws, ws['c28'])

        self.personnel = YearsRow(ws, ws['c26'])
        for i in range(len(self.personnel.years)):
            self.personnel.years[i] = self.rnd.years[i].total + self.fixed_manufactoring.years[i].total
        
        self.infrastructure = YearsRow(ws, ws['c31'])


    def parse_general_table(self):
        general_dict = {}
        names = self._ws['c2:c6']
        for name in names:
            name = name[0]
            general_dict[name.value] = ws.cell(row=name.row, column=name.col_idx + 4).value

        return general_dict

    def parse_products(self):
        prods_cells = self._ws['c12:c16']
        return [ProductRow(ws, cell[0]) for cell in prods_cells]


class OutputSheet(object):
    def __init__(self, inputs):
        # self._ws = inputs._ws
        self.products = inputs.products
        self.general_dict = inputs.general_dict 
        
        self.revenues = []
        self._init_revenues() 

        self.total_revenue_from_sales  = None
        self._init_total_revenue_from_sales()

        self.revenue_from_custome_lasers = inputs.revenue_from_custome_lasers 
        
        self.total_revenue = 0
        self._init_total_revenue()
        
        
        
    def _init_total_revenue(self):
        self.total_revenue = YearsRow(init_from_ws=False)
        self.total_revenue.name = "Total Revenue"

        year_index = 0
        for year in YEAR_RANGE:
            by = BusinnesYear(year=year, init_from_ws=False)
            
            total = 0
            for rv in [self.revenue_from_custome_lasers, self.total_revenue_from_sales]:
                total += rv.years[year_index].total
            
            by.total = total 
            self.total_revenue.years.append(by)
            year_index += 1
        
    def _init_total_revenue_from_sales(self):
        self.total_revenue_from_sales = YearsRow(init_from_ws=False)
        self.total_revenue_from_sales.name = "Total Revenue From Sales"
            
        year_index = 0
        for year in YEAR_RANGE:
            by = BusinnesYear(year=year, init_from_ws=False)
            
            total = 0
            for rv in self.revenues:
                total += rv.years[year_index].total
            by.total = total 
            self.total_revenue_from_sales.years.append(by)
            year_index += 1
            
    def _init_revenues(self):
        for prd in self.products:
            rev = ProductRow(init_from_ws=False)
            rev.name = prd.name 
            rev.KW = prd.KW
            rev.price_per_wat = prd.price_per_wat
            rev.price = prd.price
            # list type is mutable...
            rev.years = list(prd.years)
            
            for y in rev.years:
                y.q1 *= rev.price
                y.q2 *= rev.price
                y.q3 *= rev.price
                y.q4 *= rev.price
                y.calc_total()
            
            self.revenues.append(rev)


def generate_titles(ws):
    from format_utils import title_border, title_font
    titles = ['product', 'KW', 'price per watt', 'price', '2017', '2018', '2019', '2020', '2021', '2022', '2023']
    title_cells = ws['c8:m8'][0]
    for i, c in enumerate(title_cells):
        c.value = titles[i]
        c.font = title_font
        c.border = title_border
   
    
def generate_new_excel(inputs):
    wb = openpyxl.Workbook()
    
    ws = wb.active
    ws.title = "Calculated Businnes Plan"
    
    # generate_titles(ws)
    
    wb.save(filename='calculated_businnes_plan.xlsx')

    
def main():
    wb = openpyxl.load_workbook(r"C:\Users\Ronen\Downloads\Copy of short BP 2017 (1).xlsx")
    inputs = InputsSheet(wb.get_sheet_by_name('inputs'))
    
    # generate_new_excel(inputs)    
    
    return inputs
    
if __name__ == "__main__":
    # main()
    pass 